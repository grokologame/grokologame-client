import React from 'react';

function AudioElement(props) {
  return (
    <audio data-key={props.keycode || 65}>
      <source src={props.src || "sounds/Phonemes/a.wav"}/>
      Your browser doesn't do audio nodes.
    </audio>
  );
}

export default AudioElement;