import React from 'react';
import AudioElement from './AudioElement'
import { log } from '../lib/log'

/**
 * EnableSoundButton requires a callback for onClick prop and a boolean at isEnabled this.props
 * @param props
 * @param {function} props.onClick - Callback function for events
 * @param {boolean} props.isEnabled - Boolean to toggle label
 */
const EnableSoundButton = ({ onClick, isEnabled }) => (
  <button onClick={onClick}>{isEnabled ? 'Stop' : 'Start'}</button>
)

/**
 * Jambox offers sound controls via render props
 * @param props.render
 * @param props.render.ready = 
 */
  export class Jambox extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        name: "withJambox",
        audioElementData: [],
        soundIsEnabled: false
      }
      this.togglesoundIsEnabled = this.togglesoundIsEnabled.bind(this);
      this.appReady = this.appReady.bind(this);
      this.toggleAudio = this.toggleAudio.bind(this);
      this.play = this.play.bind(this);
      this.debug = false;
      this.soundBoard = {};
      this.gainNode = null;
      this.oscillator = null;
    }

    togglesoundIsEnabled() {
      this.setState((state) => {
        return {...state, soundIsEnabled: !state.soundIsEnabled }
      })
    }

    componentWillUnmount() {

    }

    play = (key) => {//TODO: check if soundIsEnabled before playing
      if (this.state.soundIsEnabled) {
        if (this.soundBoard.hasOwnProperty(key) && this.soundBoard[key].element) {
          if (this.debug) log("hit ", key, ". In soundboard I see ", this.soundBoard[key])
          this.soundBoard[key].element.currentTime = 0;
          this.soundBoard[key].element.play();
        } else {
          console.error(`Keycode ${key} is not registered in Jambox for this app.`)
        }
      } else {
        console.error(`Sound currently suspended, but key code ${ key } was seen.`)
      }
    }
    // Called when controlling app is ready, and sets up audioElementData[] which is used to render the <audio> elements
    // that the audio pipeline in createSoundBoard() connects
    /**
     * 
     * @param config Object with format { <keycode>: <audio file URI> }
     */
    appReady(config) {
      if (this.debug) log("appReady with config object: ", config);
      Object.keys(config).forEach((key) => {
        if (config[key]) {
          this.setState((state) => {
            let found = state.audioElementData.filter((item) => item.hasOwnProperty(key))
            if (found.length === 0) return { audioElementData: [...state.audioElementData, { 'key': key, src: config[key] } ] }
          })
        }
      })
    }

    // Manage creation, and pause/unpause of audio
    toggleAudio() {
      if (!this.AudioContext) {
        try {
          this.AudioContext = window.AudioContext || window.webkitAudioContext;
          this.audioContext = new AudioContext();
          this.oscillator = this.audioContext.createOscillator();
          this.gainNode = this.audioContext.createGain();
          this.setState(state => {
            return {...state,soundIsEnabled: !state.soundIsEnabled}
          })
        } catch (err) {
          console.error("Error during init: ", err)
        }
      } else {
        if (this.audioContext.state === 'suspended') {
          log("Audio enabled")
          this.audioContext.resume();
          this.togglesoundIsEnabled();
        } else {
          log("Audio suspended")
          this.audioContext.suspend();
          this.togglesoundIsEnabled();
        }
      }
    }

    /**
     * Sets up the audio context using <audio> nodes rendered out of audioElementData
     * @param config Object with format { <keycode>: <audio file URI> }
     */
    createSoundBoard = (config) => {
      const audioElementParent = document.getElementById("audioNodes");
      if (this.debug) log("creating board with ", config)
      // create sound pipeline for each audioElement set up in appReady()
      Object.keys(config).forEach((key,index,allKeys) => {
        if (this.state.soundIsEnabled && !this.soundBoard.hasOwnProperty(key)) {
          this.soundBoard[key] = Object.create({keys:allKeys},this.soundBoard[key])
          this.soundBoard[key]['element'] = audioElementParent.querySelector(`[data-key='${key}']`);
          if (this.soundBoard[key].element) {
            this.soundBoard[key]['src'] = this.audioContext.createMediaElementSource(this.soundBoard[key].element);
            this.soundBoard[key].src.connect(this.audioContext.destination);
          }
        }
      })
    }

    render() {
      const audioNodes = this.state.audioElementData.map((elem,idx) => {
        return <AudioElement key={idx} keycode={elem.key} src={elem.src} />
      })
      
      return (
        <div className="jamborder">
          {
            this.props.render({
              audioConfig: this.createSoundBoard,
              play: this.play,
              ready: this.appReady
            })
          }
          <div id="audioNodes">
            {audioNodes}
          </div>
          <EnableSoundButton isEnabled={this.state.soundIsEnabled} onClick={ this.toggleAudio }/>
        </div>
      );
    }
  }  