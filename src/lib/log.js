export const log = (shouldLog, output) => {
  if (shouldLog) {
    console.log(output)
  }
}