import React from 'react';

const Key = (p) => {
  return (
    <div data-key={p.datakey} className={`keybox${ p.active ? ' playing' :''}`}>
      <kbd>{p.letter}</kbd>
      <div>{p.children}</div>
    </div>
  );
}

export default Key;