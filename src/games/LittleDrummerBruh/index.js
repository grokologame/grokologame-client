import React from 'react';
import keytocode from 'keycode';
import Key from './Key';
import { log } from '../../lib/log';

import './index.css'

class LittleDrummerBruh extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      playing: {},
    }
    this.debug = false;
    this.audioTracks = {};
    this.enabledKeys = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "=", "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "[", "]", "a", "s", "d", "f", "g", "h", "j", "k", "l", ";", "'", "z", "x", "c", "v", "b", "n", "m", ",", "."];
    this.sounds = {
      49: "sounds/Phonemes/p.wav",
      50: "sounds/Phonemes/f.wav",
      51: "sounds/Phonemes/t.wav",
      52: "sounds/Phonemes/th.wav",
      81: "sounds/Phonemes/b.wav",
      87: "sounds/Phonemes/v.wav",
      69: "sounds/Phonemes/d.wav",
      82: "sounds/Phonemes/th-v.wav",
      65: "sounds/Phonemes/ch.wav",
      83: "sounds/Phonemes/s.wav",
      68: "sounds/Phonemes/sh.wav",
      70: "sounds/Phonemes/k.wav",
      90: "sounds/Phonemes/j.wav",
      88: "sounds/Phonemes/z.wav",
      67: "sounds/Phonemes/zh.wav",
      86: "sounds/Phonemes/g.wav",
      53: "sounds/Phonemes/h.wav",
      84: "sounds/Phonemes/m.wav",
      71: "sounds/Phonemes/n.wav",
      66: "sounds/Phonemes/ng.wav",
      78: "sounds/Phonemes/r.wav",
      77: "sounds/Phonemes/l.wav",
      188: "sounds/Phonemes/w.wav",
      190: "sounds/Phonemes/y.wav",
      54: "sounds/Phonemes/a.wav",
      55: "sounds/Phonemes/u.wav",
      56: "sounds/Phonemes/ar.wav",
      57: "sounds/Phonemes/o.wav",
      48: "sounds/Phonemes/a_r.wav",
      189: "sounds/Phonemes/i_.wav",
      187: "sounds/Phonemes/ow.wav",
      89: "sounds/Phonemes/e.wav",
      85: "sounds/Phonemes/e_.wav",
      73: "sounds/Phonemes/ir.wav",
      79: "sounds/Phonemes/or.wav",
      80: "sounds/Phonemes/ur.wav",
      219: "sounds/Phonemes/oy.wav",
      221: "sounds/Phonemes/o_.wav",
      72: "sounds/Phonemes/e_.wav",
      74: "sounds/Phonemes/i.wav",
      75: "sounds/Phonemes/oo.wav",
      76: "sounds/Phonemes/oo_.wav",
      186: "sounds/Phonemes/e_.wav",
      222: "sounds/Phonemes/a_.wav"
    };
    this.handleEvent = this.handleEvent.bind(this);
    this.tid.bind(this);
    this.triggerPlayingStyles.bind(this);
  }
  
  tid = (key) => setTimeout(() => {
     if (this.debug) log("removal timeout of ", this.state.playing[key])
    this.setState((state) => ({
      playing: {...state.playing,...{ [key]:{'enabled':false, 'id':null } }}
    }))
  }, 500)

  triggerPlayingStyles = (key)=> {
    if (!this.state.playing[key]) {
      this.setState((state) => ({
        playing: {
          ...state.playing,
            [key]: { 'enabled': true, 'id': this.tid(key) }
        }  
      }))
    } else {
      // key exists, so check if it's currently playing to see if we need to reset the timer
      if (this.state.playing[key].enabled) {
        clearTimeout(this.state.playing[key].id);
      }
        this.setState((state) => ({
          playing: { ...state.playing, ...{ [key]: { 'enabled': true, 'id': this.tid(key) } } }
        }));
    } 
  }

  handleEvent = (event) => {
    // Set up audio api with matching audio element
    if (this.debug) log("\nKEYDOWN_EVENT: ", event.keyCode)
    // ensure audio context is configured before trying to play!
    this.props.audioConfig(this.enabledKeys.reduce((collector, key) => {
      // convert letter (key) to numbered keycode
      const keycode = keytocode(key)
      collector[keycode] = this.sounds[keycode];
      return collector;
    }, {}))
    //Restrict to keycodes needed for this game
    if (this.enabledKeys.indexOf(keytocode(event.keyCode)) > -1) {
      this.props.play(event.keyCode)
      this.triggerPlayingStyles(event.keyCode)
    } else {
      console.info(`Keycode ${event.keyCode} not currently enabled for LittleDrummerBruh.`)
    }
  }
  
  componentDidMount() {
    window.addEventListener('keydown', this.handleEvent, false);
    this.props.ready(this.enabledKeys.reduce((collector, key) => {
      // convert letter (key) to numbered keycode
      const keycode = keytocode(key)
      collector[keycode] = this.sounds[keycode];
      return collector;
    }, {}));
  }
  
  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleEvent);
    // TODO: .close() all audioContexts
  }
        
  render() {
  return (
    <div id="wrap" className="wrap">{
      this.enabledKeys.map((letter) => {
        return <Key
          datakey={keytocode(letter)}
          letter={letter}
          active={ this.state.playing[keytocode(letter)] && this.state.playing[keytocode(letter)].enabled === true }
          key={ letter }
        ></Key>;
      })}
    </div>
    );
  }
}

export default LittleDrummerBruh;