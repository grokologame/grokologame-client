import './App.css';
import LittleDrummerBruh from './games/LittleDrummerBruh';
import { Jambox } from './components/Jambox';
import TicTacToe from './games/TicTacToe';

function App() {
  return (
    <div className="App">
      <Jambox render={({ audioConfig, play, ready }) => (
        <LittleDrummerBruh audioConfig={audioConfig} play={play} ready={ ready }/>
      )} />
    </div>
  );
}

export default App;
